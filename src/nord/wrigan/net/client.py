"""
Copyright 2019 NoRD Software Foundation

This file is part of NoRD.

NoRD is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

NoRD is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with NoRD.  If not, see <https://www.gnu.org/licenses/>.

--

"""

import sys
import aiohttp
import asyncio
import json

server = "http://127.0.0.1"
port = 8080


async def show_help():
    print("\n")
    print(f"{sys.argv[0]} COMMAND [args]\n"
          "  load filename - load filename mapfile for execution\n"
          "  list_maps - list the loaded maps\n"
          "  running_maps - list the running maps\n"
          "  del_map GUID - delete the map identified by guid\n"
          "  delete_all - delete all known maps\n"
          "  run GUID [arg1] [arg2] ... [argN] - execute the map identified by guid passing args.\n"
          "  start GUID [arg1] [arg2] ... [argN] - begin debugging"
          " the map identified by guid passing args.\n"
          "  next GUID - for the map identified by guid having been started, run the next node.\n"
          "  continue GUID - Request completion of the indicated map.\n"
          "  done GUID - check to see if the map with guid is done.\n"
          "  blocked GUID - check to see if the map with guid is blocked.\n"
          "  get_node GUID1 GUID2 - Get the node with id GUID2 from map with GUID1\n"
          "  hasoutput GUID1 GUID2 - Check to see if the map-node has output\n"
          "  output GUID1 GUID2 - Get the map-node has output\n"
          "  outputters GUID - Get the ids of the nodes with output.\n"
          "  wantsinput GUID1 GUID2 - Check to see if the map-node wants input\n"
          "  inputwanters GUID - Get the list of nodeid's for this map that want input\n"
          "  hasoutput GUID1 GUID2 - Check to see if the map-node has output\n"
          "  sendevent GUID1 GUID2 TYPE [PAYLOAD] - Send an event to node GUID2 in map GUID1 of type"
          " TYPE with optional payload PAYLOAD\n"
          )


async def load_file(*args, **kwargs):
    if len(args) >= 1:
        with open(args[0], 'r') as f:
            data = f.read()
            async with aiohttp.ClientSession() as session:
                async with session.post(f"{server}:{port}/map", json=json.loads(data)) as resp:
                    print(resp.status)
                    print(await resp.text())
    else:
        print("Not enough arguments to load")
        sys.exit(1)


async def list_maps(*args, **kwargs):
    async with aiohttp.ClientSession() as session:
        async with session.get(f"{server}:{port}/maps") as resp:
            print(resp.status)
            print(await resp.text())


async def delete_all(*args, **kwargs):
    async with aiohttp.ClientSession() as session:
        async with session.get(f"{server}:{port}/maps") as resp:
            d = await resp.json()
            for mapid in d["maps"]:
                async with session.delete(f"{server}:{port}/map/{mapid}") as r2:

                    print(await r2.text())
                    print(f"Deleted: {mapid}")


async def running_maps(*args, **kwargs):
    async with aiohttp.ClientSession() as session:
        async with session.get(f"{server}:{port}/maps/running") as resp:
            print(resp.status)
            print(await resp.text())


async def del_map(*args, **kwargs):
    async with aiohttp.ClientSession() as session:
        async with session.delete(f"{server}:{port}/map/{args[0]}") as resp:
            print(resp.status)
            print(await resp.text())


async def run_map(*args, **kwargs):
    async with aiohttp.ClientSession() as session:
        payload = args[1:]
        async with session.put(f"{server}:{port}/map/run/{args[0]}", json=payload) as resp:
            print(resp.status)
            print(await resp.text())


async def continue_exec(*args, **kwargs):
    async with aiohttp.ClientSession() as session:
        async with session.put(f"{server}:{port}/map/continue/{args[0]}") as resp:
            print(resp.status)
            print(await resp.text())


async def start_map(*args, **kwargs):
    async with aiohttp.ClientSession() as session:
        payload = args[1:]
        async with session.put(f"{server}:{port}/map/start/{args[0]}", json=payload) as resp:
            print(resp.status)
            print(await resp.text())


async def next_node(*args, **kwargs):
    async with aiohttp.ClientSession() as session:
        async with session.put(f"{server}:{port}/next/{args[0]}") as resp:
            print(resp.status)
            print(await resp.text())


async def get_node(*args, **kwargs):
    async with aiohttp.ClientSession() as session:
        if len(args) < 2:
            print("Not enough arguments to get_node")
            sys.exit(1)

        async with session.get(f"{server}:{port}/node/{args[0]}/{args[1]}") as resp:
            print(resp.status)
            print(await resp.text())


async def is_done(*args, **kwargs):
    async with aiohttp.ClientSession() as session:
        async with session.get(f"{server}:{port}/maps/done/{args[0]}") as resp:
            print(resp.status)
            print(await resp.text())


async def is_blocked(*args, **kwargs):
    async with aiohttp.ClientSession() as session:
        async with session.get(f"{server}:{port}/map/blocked/{args[0]}") as resp:
            print(resp.status)
            print(await resp.text())


async def has_output(*args, **kwargs):
    async with aiohttp.ClientSession() as session:
        async with session.options(f"{server}:{port}/output/{args[0]}/{args[1]}") as resp:
            print(resp.status)
            print(await resp.text())


async def get_outputters(*args, **kwargs):
    async with aiohttp.ClientSession() as session:
        async with session.get(f"{server}:{port}/output/{args[0]}") as resp:
            print(resp.status)
            print(await resp.text())


async def get_output(*args, **kwargs):
    async with aiohttp.ClientSession() as session:
        async with session.get(f"{server}:{port}/output/{args[0]}/{args[1]}") as resp:
            print(resp.status)
            print(await resp.text())


async def wants_input(*args, **kwargs):
    async with aiohttp.ClientSession() as session:
        async with session.options(f"{server}:{port}/input/{args[0]}/{args[1]}") as resp:
            print(resp.status)
            print(await resp.text())


async def input_wanters(*args, **kwargs):
    async with aiohttp.ClientSession() as session:
        async with session.get(f"{server}:{port}/input/{args[0]}") as resp:
            print(resp.status)
            print(await resp.text())


async def put_input(*args, **kwargs):
    if len(args) < 3:
        print("Not enough arguments to input")
        sys.exit(1)

    payload = args[2:]

    async with aiohttp.ClientSession() as session:
        async with session.post(f"{server}:{port}/input/{args[0]}/{args[1]}", json=payload) as resp:
            print(resp.status)
            print(await resp.text())


async def send_event(*args, **kwargs):
    if len(args) < 3:
        print("Not enough arguments to input")
        sys.exit(1)

    if len(args) > 4:
        dat = " ".join(args[3:])
    else:
        dat = None
    payload = {
        "node_id": args[1],
        "type": args[2],
        "payload": dat
    }

    async with aiohttp.ClientSession() as session:
        async with session.put(f"{server}:{port}/event/{args[0]}", json=payload) as resp:
            print(resp.status)
            print(await resp.text())


cmd2func = {
    'load': load_file,
    'list_maps': list_maps,
    "running_maps": running_maps,
    'del_map': del_map,
    "run": run_map,
    "start": start_map,
    "next": next_node,
    "continue": continue_exec,
    "get_node": get_node,
    "done": is_done,
    "blocked": is_blocked,
    "hasoutput": has_output,
    "outputters": get_outputters,
    "output": get_output,
    "wantsinput": wants_input,
    "inputwanters": input_wanters,
    "input": put_input,
    "delete_all": delete_all,
    "sendevent": send_event,
    'help': show_help
}


def run_client():
    if len(sys.argv) < 2:
        print("Command must be specified.")
        sys.exit(1)

    cmd = sys.argv[1]
    if len(sys.argv) >= 3:
        args = sys.argv[2:]
    else:
        args = []

    loop = asyncio.get_event_loop()
    func = cmd2func.get(cmd.lower(), None)
    if func is None:
        print(f"Unknown command: {cmd} requested.")
        loop.run_until_complete(show_help())
        sys.exit(1)

    else:
        loop.run_until_complete(func(*args))
