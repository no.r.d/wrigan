"""
Copyright 2019 NoRD Software Foundation

This file is part of NoRD.

NoRD is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

NoRD is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with NoRD.  If not, see <https://www.gnu.org/licenses/>.

--

This is the daemon which exposes the walujapi runtime.

It facilitates communication with Sigurd frontend. It also
provides the visibles and menuitems to Sigurd to interact with
this backend.


At some point some authentication and security will need to be added to this.
Perhaps by way of a proxy / intermediate layer. Perhaps in this extension.


How it works:

A command is executed, and the server acknowledges it.
Some commands (those with a * will ack with additional data, and do not need to be polled)
The client then must poll the status endpoint to know the command completed successfully.

Commands are:
  - load_map*
     - client sends JSON of map
     - server acknowledges with guid to reference map
  - list_maps*
     - client sends request
     - server responds with a list of all known maps, and their current state.
  - run_map
     - client sends guid to run
     - server acks if guid known with a state GUID
     - client polls get_status with state GUID
     - server returns map state
  - start_map*
     - client sends a guid to start in debug mode
     - server acks if guid known with a state GUID
     - client polls get_status with state GUID
     - server returns map state
  - list_running_maps*
     - client requests list of running maps
     - server returns list of guids
  - get_node*
     - client sends running map state GUID and Node guid
     - server acks with node information.... (needs to be implemented in walujapi)
  - get_output*
     - client sends map state GUID, and the guid from has_output state it wants the output from
     - server responds with the output data.
  - put_input*
     - client sends the map state GUID and the guid from needs_input and the input data
     - the server acknowledges
  - next_node
     - client sends the map state GUID
     - server acknowledges


map state
  - running
  - complete
  - anomaly
  - failed
  - needs_input
  - has_output
  - dbg_waiting

The execution server is an intermediary system. It communicates with
sigurd frontends on one side, and with an arbitrary number of
walujapi backend processes on the other side. Wrigan contains the code
which wraps walujapi in an asyncio TCP server.

The wrapped walujapi functionality mirrors that of the server above except
it does not allow for list_maps or list_running_maps.

"""

from aiohttp import web
import logging
import nord.nord.Map as Map
from nord.nord.Runtime import Runtime
import json
import asyncio
import uvloop
import nord.pasion.visibles.wallet as wallet
from nord.sushrut.utils import prepare_output
from nord.wrigan.net.utils import gen_map_change_catcher,\
    clear_map_change_catcher,\
    get_changes,\
    catcher_has_changes
import nord.sigurd.utils.namegen as ng


asyncio.set_event_loop_policy(uvloop.EventLoopPolicy())


logging.basicConfig(level=logging.DEBUG)


def lrv(d):
    print(json.dumps(d, indent=4))
    return d


def request_extract_runtime(request):
    vessal, _ = request_extract_map_runtime(request)
    return vessal


def request_extract_map(request, ok_not_run=True):
    _, karte = request_extract_map_runtime(request, ok_not_run=ok_not_run)
    return karte


def request_extract_map_runtime(request, ok_not_run=False, mapid="id"):
    guid = request.match_info.get(mapid, None)
    if guid is None:
        logging.error(f"missing map id in request: {request}")
        raise web.HTTPBadRequest(text="Missing map ID")

    vessal = None
    app = request.app
    if guid not in app["maps"]:
        logging.error(f"unknown map in request: {request}")
        raise web.HTTPPreconditionFailed(text="Unknown map")
    if guid not in app["runtimes"] and not ok_not_run:
        logging.error(f"map not running in request: {request}")
        raise web.HTTPPreconditionFailed(text="Map not running")
    elif not ok_not_run and guid in app["runtimes"]:
        vessal = app["runtimes"][guid]

    return vessal, app["maps"][guid]


def request_extract_node_map_runtime(request, ok_not_run=False, mapid="id"):
    nodeid = request.match_info.get('node', None)
    if nodeid is None:
        logging.error(f"Missing node id in request: {request}")
        raise web.HTTPBadRequest(text="Missing node ID")

    vessal, karte = request_extract_map_runtime(request, ok_not_run=ok_not_run, mapid=mapid)

    node = karte.get_node(nodeid)
    return vessal, karte, node


def request_extract_edge_map_runtime(request, ok_not_run=False, mapid="id"):
    edgeid = request.match_info.get('edge', None)
    if edgeid is None:
        logging.error(f"Missing edge id in request: {request}")
        raise web.HTTPBadRequest(text="Missing edge ID")

    edgeid = tuple(edgeid.split(':'))
    if len(edgeid) != 3:
        logging.error(f"Malformed edge id in request: {request}")
        raise web.HTTPBadRequest(text="Malformed edge ID")

    vessal, karte = request_extract_map_runtime(request, ok_not_run=ok_not_run, mapid=mapid)

    edge = karte.get_edge(*edgeid)
    return vessal, karte, edge


async def load_map(request):
    data = await request.json()

    karte = Map(data, verbose=True)
    karte.connect_cdc_loopback()  # so that runtime changes manifest within the loaded graph.
    # perhaps this is one of those things to separate out in some way
    karte.add_change_handler(gen_map_change_catcher(karte))
    app = request.app

    guid = ng.id()
    app["maps"][guid] = karte
    return web.json_response(lrv({"guid": guid}))


async def list_maps(request):
    app = request.app

    app["maps"]
    return web.json_response(lrv({"maps": [x for x in app["maps"].keys()]}))


async def del_map(request):
    guid = request.match_info.get('id', None)
    if guid is None:
        raise web.HTTPBadRequest()

    app = request.app
    if guid not in app["maps"]:
        raise web.HTTPPreconditionFailed(text="Unknown map")

    karte = app["maps"][guid]
    import nord.pasion.managers.sessionmanager as sm
    if sm.get_current_session() is not None:
        wallet.settle_session(karte)

    del app["maps"][guid]

    if guid in app["runtimes"]:
        del app["runtimes"][guid]

    if guid in app["run_tasks"]:
        app["run_tasks"][guid].cancel()
        del app["run_tasks"][guid]

    clear_map_change_catcher(karte)

    raise web.HTTPOk()


async def run_map(request):
    guid = request.match_info.get('id', None)
    if guid is None:
        raise web.HTTPBadRequest()

    app = request.app
    if guid not in app["maps"]:
        raise web.HTTPPreconditionFailed(text="Unknown map")

    await request.json()

    karte = app["maps"][guid]
    if guid in app["runtimes"]:
        del app["runtimes"][guid]

    vessal = Runtime()
    app["runtimes"][guid] = vessal

    vessal.initialize(karte, verbose=karte.verbose)
    async_task = asyncio.ensure_future(vessal.begin_async())

    if guid in app["run_tasks"]:
        app["run_tasks"][guid].cancel()
    app["run_tasks"][guid] = async_task

    raise web.HTTPOk()


async def start_map(request):
    guid = request.match_info.get('id', None)
    if guid is None:
        raise web.HTTPBadRequest(text="Missing map ID")

    app = request.app
    if guid not in app["maps"]:
        raise web.HTTPPreconditionFailed(text="Unknown map")

    await request.json()

    karte = app["maps"][guid]
    if guid in app["runtimes"]:
        del app["runtimes"][guid]

    vessal = Runtime()
    app["runtimes"][guid] = vessal

    vessal.initialize(karte, verbose=True)
    vessal.pause()
    vessal.begin(debug=True)

    return web.json_response(lrv(vessal.get_current_node().to_dict()))


async def debug_map_to_break(request):
    guid = request.match_info.get('id', None)
    if guid is None:
        raise web.HTTPBadRequest(text="Missing map ID")
    vessal = request_extract_runtime(request)

    app = request.app
    if guid not in app["maps"]:
        raise web.HTTPPreconditionFailed(text="Unknown map")

    await request.json()

    karte = app["maps"][guid]

    if vessal is None or (vessal is not None and vessal.anomaly is None and vessal.stopped):
        # This branch will start the Map over from the beginning.
        if guid in app["runtimes"]:
            del app["runtimes"][guid]
        vessal = Runtime()
        app["runtimes"][guid] = vessal

        vessal.initialize(karte, verbose=True)
        vessal.begin(debug=True)
    elif vessal.anomaly is None and not vessal.stopped:
        vessal.continue_exec()
    return web.json_response(lrv(vessal.get_current_node().to_dict()))


async def continue_map(request):

    vessal = request_extract_runtime(request)
    guid = request.match_info.get('id', None)
    async_task = asyncio.ensure_future(vessal.continue_async())
    app = request.app

    if guid in app["run_tasks"]:
        app["run_tasks"][guid].cancel()
    app["run_tasks"][guid] = async_task

    return web.HTTPOk()


async def list_running_maps(request):
    app = request.app

    app["runtimes"]
    return web.json_response(lrv({"maps": [x for x in app["runtimes"].keys()]}))


async def get_node(request):
    vessal, karte, node = request_extract_node_map_runtime(request, mapid="map")
    if node is None:
        raise web.HTTPNotFound(text=f"Unknown node: {request.match_info.get('node', 'UNK')} in get_node")

    return web.json_response(lrv(node.to_dict()))


async def next_node(request):
    vessal = request_extract_runtime(request)

    if vessal.has_anomaly():
        raise web.HTTPInternalServerError(text=str(vessal.get_anomaly()))

    vessal.next()
    node = vessal.get_current_node()
    if node is not None:
        return web.json_response(lrv(node.to_dict()))
    else:
        return web.json_response(lrv({}))


async def is_blocked(request):
    vessal = request_extract_runtime(request)

    return web.json_response(lrv({"blocked": vessal.is_blocked()}))


async def is_paused(request):
    vessal = request_extract_runtime(request)

    return web.json_response(lrv({"paused": vessal.is_paused()}))


async def map_changed(request):
    karte = request_extract_map(request)

    rv = catcher_has_changes(karte)

    return web.json_response(lrv({"changed": rv}))


async def map_changes(request):
    karte = request_extract_map(request)
    karte.ship_changes()
    changes = get_changes(karte)
    return web.json_response(lrv(changes))


async def apply_map_changes(request):
    karte = request_extract_map(request)
    data = await request.json()
    lrv(data)
    karte.apply_changes(data)
    return web.json_response(lrv({"applied": True}))


async def node_debug_info(request):
    vessal, karte, node = request_extract_node_map_runtime(request, mapid="map")
    if node is None:
        raise web.HTTPNotFound(text=f"Unknown node: {request.match_info.get('node', 'UNK')} in get_node")

    return web.json_response(lrv(node.debug_to_dict()))


async def edge_debug_info(request):
    vessal, karte, edge = request_extract_edge_map_runtime(request, mapid="map")
    if edge is None:
        raise web.HTTPNotFound(text=f"Unknown node: {request.match_info.get('edge', 'UNK')} in get_edge")

    return web.json_response(lrv(edge.debug_to_dict()))


async def get_executed_elements(request):
    vessal, karte = request_extract_map_runtime(request, mapid="map")
    return web.json_response(lrv(vessal.get_executed()))


async def is_done(request):
    guid = request.match_info.get('id', None)
    if guid is None:
        raise web.HTTPBadRequest(text="Missing map ID")

    app = request.app
    if guid not in app["maps"]:
        raise web.HTTPPreconditionFailed(text="Unknown map")

    if guid not in app["runtimes"]:
        return web.json_response({"done": False})
    vessal = app["runtimes"][guid]

    if vessal.has_anomaly():
        raise web.HTTPInternalServerError(text=str(vessal.get_anomaly()))

    return web.json_response(lrv({"done": vessal.is_done()}))


async def has_output(request):
    vessal, karte, node = request_extract_node_map_runtime(request)
    if node is None:
        raise web.HTTPUnprocessableEntity(text=f"Unknown node: {node.id} in has_output")

    return web.json_response(lrv({"has_output": vessal.has_output(node.id)}))


async def wants_input(request):
    vessal, karte, node = request_extract_node_map_runtime(request)
    if node is None:
        raise web.HTTPUnprocessableEntity(text=f"Unknown node: {node.id} in wants_input")

    return web.json_response(lrv({"wants_input": vessal.wants_input(node.id)}))


async def input_wanters(request):
    vessal = request_extract_runtime(request)
    return web.json_response(lrv({"wants_input": vessal.input_wanters()}))


async def get_outputters(request):
    vessal = request_extract_runtime(request)

    return web.json_response(lrv({"has_output": vessal.get_outputters()}))


async def get_output(request):
    vessal, karte, node = request_extract_node_map_runtime(request)
    if node is None:
        raise web.HTTPUnprocessableEntity(text=f"Unknown node: {node.id} in get_output")

    output = prepare_output(vessal.get_output(node.id), karte)
    try:
        return web.json_response(lrv({"output": output}))
    except TypeError:
        return web.json_response(lrv({"output": repr(output)}))


async def put_input(request):
    vessal, karte, node = request_extract_node_map_runtime(request)
    if node is None:
        raise web.HTTPUnprocessableEntity(text=f"Unknown node: {node.id} in put_input")

    data = await request.json()
    lrv(data)

    vessal.set_input(node.id, data)
    raise web.HTTPOk()


async def communicate(request):
    vessal, karte, node = request_extract_node_map_runtime(request, ok_not_run=True)
    if node is None:
        raise web.HTTPUnprocessableEntity(text=f"Unknown node: {node.id} in put_input")

    data = await request.json()
    lrv(data)

    response = node.handle_visible_request(data)

    return web.json_response(lrv(response))


async def view_current(request):
    vessal = request_extract_runtime(request)
    rv = vessal.export_as_graphviz()
    return web.json_response(lrv({"view": rv}))


async def get_events(request):
    vessal = request_extract_runtime(request)

    return web.json_response(lrv({"events": vessal.get_out_events()}))


async def add_event(request):
    vessal = request_extract_runtime(request)

    data = await request.json()
    lrv(data)

    vessal.add_in_event(data)

    # ### What do we do here?
    # ### Presumably, call next if the map is not in the
    # ### middle of anything, otherwise,
    # ### it should pick up the event and deal with it...?
    if vessal.is_done() or vessal.is_paused():
        vessal.unpause()
        asyncio.ensure_future(vessal.continue_async())

    raise web.HTTPOk()


async def get_wallet_address(request):
    address = wallet.get_address()

    return web.json_response(lrv({"address": address}))


async def get_last_tx(request):
    address = request.match_info.get('address', None)
    wallet.load_blockchain()

    # Given the address, get the last txid
    # from the UTXO
    transaction, index = wallet.get_last_txid(address)

    return web.json_response(lrv({"txid": transaction, "index": index}))


async def get_blockchain_tx(request):
    import nord.pasion.visibles.wallet as w
    txid = request.match_info.get('txid', None)
    wallet.load_blockchain()

    return web.Response(text=w.get_tx(txid))


def run_server():
    # Generate a new wallet, if one is not present
    import sys
    if len(sys.argv) > 1:
        port = int(sys.argv[1])
    else:
        port = 8080
    wallet.establish_address('/tmp/wrigan.pkey', 'Secret JUST for Wrigan')
    wallet.load_blockchain()
    app = web.Application()
    app['maps'] = {}
    app["runtimes"] = {}
    app["run_tasks"] = {}

    app.add_routes([web.post('/map', load_map),
                    web.get('/maps', list_maps),
                    web.get('/wallet_address', get_wallet_address),
                    web.get('/last_tx_for/{address}', get_last_tx),
                    web.get('/tx/{txid}.hex', get_blockchain_tx),
                    web.delete('/map/{id}', del_map),
                    web.put('/map/run/{id}', run_map),
                    web.put('/map/start/{id}', start_map),
                    web.put('/map/debug_continue/{id}', debug_map_to_break),
                    web.get('/maps/running', list_running_maps),
                    web.put('/map/continue/{id}', continue_map),
                    web.get('/map/blocked/{id}', is_blocked),
                    web.get('/map/paused/{id}', is_paused),
                    web.get('/map/changed/{id}', map_changed),
                    web.get('/map/changes/{id}', map_changes),
                    web.post('/map/changes/{id}', apply_map_changes),
                    web.get("/map/{map}/debugnode/{node}", node_debug_info),
                    web.get("/map/{map}/debugedge/{edge}", edge_debug_info),
                    web.get("/map/{map}/executed", get_executed_elements),
                    web.get('/map/done/{id}', is_done),
                    web.get('/node/{map}/{node}', get_node),
                    web.options('/output/{id}/{node}', has_output),
                    web.get('/output/{id}/{node}', get_output),
                    web.get('/output/{id}', get_outputters),
                    web.options('/input/{id}/{node}', wants_input),
                    web.get('/input/{id}', input_wanters),
                    web.post('/input/{id}/{node}', put_input),
                    web.post('/communicate/{id}/{node}', communicate),
                    web.put('/next/{id}', next_node),
                    web.get('/events/{id}', get_events),
                    web.get('/view_current_rt/{id}', view_current),
                    web.put('/event/{id}', add_event)])
    web.run_app(app, port=port)
