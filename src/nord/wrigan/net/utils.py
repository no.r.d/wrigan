"""
Copyright 2019 NoRD Software Foundation

This file is part of NoRD.

NoRD is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

NoRD is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with NoRD.  If not, see <https://www.gnu.org/licenses/>.

--

"""
from nord.sigurd.utils.log import debug, err
import requests

server = "http://127.0.0.1"
port = 8080
_change_handlers = {}


def _send(method, url, json=None):
    """Utility function wrapping requests to output url and status code."""
    mfuncs = {
        'GET': requests.get,
        'PUT': requests.put,
        'POST': requests.post,
        'DELETE': requests.delete
    }

    url = f"{server}:{port}/{url}"

    func = mfuncs[method]
    if json is not None:
        resp = func(url, json=json)
    else:
        resp = func(url)
    debug(f"{method} - {url} - payload: {json}- resulted in {resp.status_code}")
    return resp


def get(url, data=None):
    return _send('GET', url, json=data)


def put(url, data=None):
    return _send('PUT', url, json=data)


def post(url, data):
    return _send('POST', url, json=data)


def delete(url, data=None):
    return _send('DELETE', url, json=data)


def gen_map_change_catcher(karte):
    """
    Prepare the function to capture all the map changes
    for the given map, to be sent to the client upon request.
    """
    global _change_handlers
    if karte in _change_handlers:
        err(f"{karte} already registered for change capture")
        return

    def change_catcher(change):
        global _change_handlers
        if change:
            _change_handlers[karte][1].extend(change)

    _change_handlers[karte] = (change_catcher, [])
    return change_catcher


def catcher_has_changes(karte):
    global _change_handlers
    if karte not in _change_handlers:
        return False
    else:
        return len(_change_handlers[karte][1]) > 0


def get_changes(karte):
    global _change_handlers
    if karte not in _change_handlers:
        err(f"{karte} not registered for change capture")
        return []
    else:
        changes = [x for x in _change_handlers[karte][1]]
        _change_handlers[karte][1].clear()
        return changes


def clear_map_change_catcher(karte):
    """
    remove the change handler for the requested map
    """
    global _change_handlers
    if karte not in _change_handlers:
        err(f"{karte} not registered for change capture")
        return
    else:
        del _change_handlers[karte]
