"""
Copyright 2019 NoRD Software Foundation

This file is part of NoRD.

NoRD is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

NoRD is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with NoRD.  If not, see <https://www.gnu.org/licenses/>.

--

"""
from nord.sigurd.utils.verboser import funlog  # noqa: F401
import nord.sigurd.managers.objectmanager as om
from nord.wrigan.net.utils import post, get
import nord.sigurd.utils.namegen as ng
from nord.sigurd.config.globalVars import DEFAULTTTDBGVISSECS
from nord.sigurd.managers.event import callback


_dbg_tipped_nodes = {}
_dbg_tipped_edges = {}
_debug_menu = None


@funlog
def dim_map():
    import nord.sigurd.shell.vismap as Map
    for nid in Map.get_nodes():
        node = om.fetch(nid)
        if hasattr(node, 'set_dimmed'):
            node.set_dimmed()


@funlog
def undim_map():
    import nord.sigurd.shell.vismap as Map
    for nid in Map.get_nodes():
        node = om.fetch(nid)
        if hasattr(node, 'set_undimmed'):
            node.set_undimmed()


@funlog
def show_error(text):
    """Render the passed text as on error and delete the map if possible."""
    print(f"RUNTIME ERROR: {text}")
    from nord.sigurd.visibles.anomaly import Anomaly
    import nord.sigurd.shell.application as App
    anomaly = Anomaly(ng.id(), App)
    om.add_item(App.get_onscreen_pos(), App, anomaly)
    anomaly.setLabelText(text)


@funlog
def move_cam_to(pos):
    """Move the camera to the provided position"""
    import nord.sigurd.shell.application as App
    from panda3d.core import Point3
    App.anim_move_cam_to(Point3(pos[0], pos[1], pos[2]))


# This is called in callbackmanager's mapentry_comm method
@funlog
def communicate(map_id, nodeid, payload):
    resp = post(f"communicate/{map_id}/{nodeid}", payload)
    if resp.status_code == 200:
        return resp.json()
    else:
        show_error(resp.text)
        return None


# @funlog
def get_changes(map_id, filteruuid=None):
    resp = get(f"map/changes/{map_id}")
    if resp.status_code == 200:
        rv = resp.json()
        if filteruuid is not None:
            rv = [x for x in rv if x.get("change_src_id", "NO") != filteruuid]
        return rv
    else:
        show_error(resp.text)
        return None


# @funlog
def ship_changes(map_id, change_dict):
    resp = post(f"map/changes/{map_id}", change_dict)
    if resp.status_code == 200:
        return resp.json()
    else:
        show_error(resp.text)
        return None


# @funlog
def get_node_dbg_info(txt, map_id, node):
    resp = get(f"map/{map_id}/debugnode/{node.uid}")
    if resp.status_code == 200:
        dat = resp.json()
        txt, executed = node.mapentry.render_dbg_text(txt, dat)
    else:
        txt += "TT UPDATE ERROR: " + resp.text
        executed = False
    return txt, executed


@funlog
def gen_node_dbg_tt_fetcher(map_id, node):
    orig_tt_func = node.get_tooltip_updater()

    def dbg_node_tt_fetcher(tt):
        orig_tt_func(tt)
        txt = tt.get_text()
        txt, _ = get_node_dbg_info(txt, map_id, node)

        tt.set_text(txt)
    # return the generated debug function
    return dbg_node_tt_fetcher


# @funlog
def get_edge_dbg_info(txt, map_id, edge):
    # fetch the additional runtime data from the backend and
    # and generate a string from it.
    if hasattr(edge, 'visualization'):
        edge = edge.visualization.cEdge
    if not hasattr(edge, 'uid'):
        return '', False
    resp = get(f"map/{map_id}/debugedge/{edge.uid}")
    if resp.status_code == 200:
        dat = resp.json()
        txt, followed = edge.connection.mapentry.render_dbg_text(txt, dat)
    else:
        txt += "\n\nTT UPDATE ERROR:\n\t" + resp.text
        followed = False
    return txt, followed


@funlog
def gen_edge_dbg_tt_fetcher(map_id, edge):
    orig_tt_func = edge.get_tooltip_updater()

    def dbg_tt_edge_fetcher(tt):
        orig_tt_func(tt)
        txt = tt.get_text()
        txt, _ = get_edge_dbg_info(txt, map_id, edge)

        tt.set_text(txt)
    # return the generated debug function
    return dbg_tt_edge_fetcher


@funlog
def inject_dbg_tooltippers(map_id):
    """
    for the vismap provided, add the debug tooltipper
    to every model.
    """
    global _dbg_tipped_nodes
    if _dbg_tipped_nodes:
        return
    import nord.sigurd.shell.vismap as Map
    for nid in Map.get_nodes():
        node = om.fetch(nid)
        _dbg_tipped_nodes[nid] = (node.get_tooltip_updater(), node.get_tooltip_vis_secs())
        node.set_tooltip_updater(gen_node_dbg_tt_fetcher(map_id, node))
        node.set_tooltip_vis_secs(DEFAULTTTDBGVISSECS)
    for eid in Map.get_edges():
        eidstr = f"{eid[0]}:{eid[1]}:{eid[2]}"
        if om.has(eidstr):
            edge = om.fetch(eidstr)
            _dbg_tipped_nodes[eidstr] = (edge.get_tooltip_updater(), edge.get_tooltip_vis_secs())
            edge.set_tooltip_updater(gen_edge_dbg_tt_fetcher(map_id, edge))
            edge.set_tooltip_vis_secs(DEFAULTTTDBGVISSECS)


@funlog
def clear_injected_tooltippers():
    """
    Restore all model's tooltippers to their original state...
    """
    global _dbg_tipped_nodes
    if _dbg_tipped_nodes:
        for nid in _dbg_tipped_nodes:
            (func, secs) = _dbg_tipped_nodes[nid]
            node = om.fetch(nid)
            node.set_tooltip_updater(func)
            node.set_tooltip_vis_secs(secs)
            node.renderUnselected()
    _dbg_tipped_nodes.clear()


@callback
@funlog
def show_debug_menu(*args, **kwargs):
    global _debug_menu
    if _debug_menu is None:
        from nord.sigurd.shell.menu import Menu
        _debug_menu = Menu(space="wrigan")
        _debug_menu.load_from_file('config/menu.conf', 'wrigan')
    _debug_menu.show(*args, **kwargs)


@callback
@funlog
def hide_debug_menu(*args, **kwargs):
    global _debug_menu
    if _debug_menu is None:
        from nord.sigurd.shell.menu import Menu
        _debug_menu = Menu(space="wrigan")
        _debug_menu.load_from_file('config/menu.conf', 'wrigan')
    _debug_menu.hide(*args, **kwargs)


@callback
@funlog
def debug_update_container(*args, **kwargs):
    global _debug_menu
    if _debug_menu is None:
        from nord.sigurd.shell.menu import Menu
        _debug_menu = Menu(space="wrigan")
        _debug_menu.load_from_file('config/menu.conf', 'wrigan')
    _debug_menu.update_container()
