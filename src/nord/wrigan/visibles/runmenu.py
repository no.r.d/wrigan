"""
Copyright 2019 NoRD Software Foundation

This file is part of NoRD.

NoRD is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

NoRD is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with NoRD.  If not, see <https://www.gnu.org/licenses/>.

--

"""

from nord.sigurd.utils.verboser import funlog  # noqa: F401
import nord.sigurd.shell.vismap as Map
import nord.sigurd.managers.objectmanager as om
import nord.sigurd.managers.event as event
import nord.sigurd.managers.statemanager as em
import time
from nord.sigurd.utils.log import debug
from nord.wrigan.net.utils import get, put, post, delete
from nord.wrigan.visibles.utils import dim_map,\
                                                                undim_map,\
                                                                show_error,\
                                                                inject_dbg_tooltippers,\
                                                                clear_injected_tooltippers,\
                                                                get_node_dbg_info,\
                                                                get_edge_dbg_info,\
                                                                move_cam_to
import nord.pasion.visibles.wallet as wallet


CONFIG_MAP_TIMEOUT = 5
CONFIG_ITER_WAITTIME = 1.0

_running_map = None
_last_node = None
_hw_address = None
_prev_idx = 0
_prev_tx = None


@funlog
def get_running_map_id():
    global _running_map
    return _running_map


@funlog
def fetch_and_send_input(nodeid, indebug=False):
    node = om.fetch(nodeid)
    node.move_cam_to()
    node.set_undimmed()

    def gen_cbk():
        n = node
        nid = nodeid

        @event.callback
        @funlog
        def on_provided(*args, **kwargs):
            n.set_dimmed()
            payload = args[0]
            resp = post(f"input/{_running_map}/{nid}", payload)
            if resp.status_code == 200 and not indebug:
                r2 = put(f"map/continue/{_running_map}")
                if r2.status_code == 200:
                    continue_map()
                else:
                    # indicate run failure to the user
                    show_error(resp.text + "\nUnable to continue Map execution")
                    return
            elif not indebug:
                # indicate the failure to the user
                show_error(resp.text + "\nUnable to deliver required input.")
                return
        return on_provided

    node.show_prompt(gen_cbk())


@funlog
def gather_show_outputs():
    global _running_map

    """
    Outbound events, currently, are only used by
    Athena.... But, they are facilitated by Walujapi and Wrigan
    """
    Map.synchronize()
    return
    resp = get(f"events/{_running_map}")
    if resp.status_code == 200:
        d = resp.json()
        for evnt in d["events"]:
            nodeid = evnt["node_id"]
            node = om.fetch(nodeid)
            if hasattr(node, "handle_event"):
                node.handle_event(evnt)
            else:
                show_error(f"IMPLEMENTATION ERROR: Event Incapable node received: {evnt}")
                em.onmapstop()
                return
    else:
        show_error(resp.text + f"\nFailed to fetch out events for map:{_running_map}")
        em.onmapstop()
        return

    resp = get(f"output/{_running_map}")
    if resp.status_code == 200:
        d = resp.json()
        for nodeid in d["has_output"]:
            resp = get(f"output/{_running_map}/{nodeid}")
            if resp.status_code == 200:
                node = om.fetch(nodeid)
                node.show_output(resp.text)
                node.move_cam_to()
                node.set_undimmed()
            else:
                show_error(resp.text + f"\nFailed to fetch output for:\n{nodeid}")
                em.onmapstop()
                return
    else:
        show_error(resp.text + f"\nFailed to fetch outputs for map:{_running_map}")
        em.onmapstop()
        return


@funlog
def run_map(*args, **kwargs):
    global _running_map, _last_node, _hw_address, _prev_tx, _prev_idx

    em.onmapstart()
    dim_map()

    # load the map
    data = Map.get_map_dict()  # get the current map.

    if wallet.get_address() is not None:
        if _hw_address is None:
            resp = get("wallet_address")
            if resp.status_code != 200:
                show_error(resp.text)
                return
            else:
                _hw_address = resp.json()["address"]

                if _prev_tx is None:
                    # Request the last transaction ID
                    # from the server, if none exists, user
                    # has no funds....
                    resp = get(f"last_tx_for/{wallet.get_address()}")
                    if resp.status_code != 200:
                        show_error(resp.text)
                        return
                    tx_dat = resp.json()
                    if "txid" not in tx_dat or tx_dat["txid"] is None:
                        show_error("No funds available")
                        return
                    _prev_tx = bytes.fromhex(tx_dat["txid"])
                    _prev_idx = tx_dat["index"]

                data["payment_to"] = _hw_address
                data["refunds_to"] = wallet.get_address()
                data["signed_tx_block"], _prev_tx = wallet.pay_for_map(data,
                                                                       _hw_address,
                                                                       _prev_tx,
                                                                       _prev_idx)
                _prev_idx = 0

    resp = post("map", data)
    print(resp.status_code)
    if resp.status_code != 200:
        show_error(resp.text)
        return
    d = resp.json()
    _running_map = d["guid"]

    # run the map to completion, or blocking...
    resp = put(f"map/run/{_running_map}", data=[])
    print(resp.status_code)
    print(resp.text)

    continue_map()

@funlog
def render_exec_path():
    global _running_map, _last_node

    for node in Map.get_nodes():
        if hasattr(node, "visualization"):
            node.visualization.renderUnselected()
    for edge in Map.get_edges():
        if hasattr(edge, "visualization"):
            edge.visualization.renderUnselected()
    # get the list of executed edges and nodes
    # and visualize their status
    resp = get(f"map/{_running_map}/executed")
    if resp.status_code != 200:
        show_error(resp.text)
        em.onmapstop()
        return

    d = resp.json()
    if d["last_node"]:
        if om.has(d["last_node"]):
            node = om.fetch(d["last_node"])
            node.set_undimmed()
            node.move_cam_to()

            _last_node = node

        for nid in d["nodes"]:
            if om.has(nid):
                n = om.fetch(nid)
                n.renderSelected()

        for eid in d["edges"]:
            e = Map.get_edges()[(eid[0], eid[1], eid[2])]
            if hasattr(e, "visualization"):
                e.visualization.renderSelected()



@funlog
def dbg_run_to_break(*args, **kwargs):
    global _running_map, _last_node
    resp = put(f"map/debug_continue/{_running_map}", data=[])
    print(resp.status_code)
    print(resp.text)
    just_stop, done, blocked, paused = check_map_runstatus()

    render_exec_path()


@funlog
def check_map_runstatus():
    # check for i/o
    # if input is needed, request it from the appropriate visible
    resp = get(f"input/{_running_map}")
    if resp.status_code != 200:
        show_error(resp.text)
        em.onmapstop()
        return True, False, False, False

    d = resp.json()
    if len(d['wants_input']) > 0:
        for nodeid in d['wants_input']:
            fetch_and_send_input(nodeid)
        return True, False, False, False

    # check to see if the map is paused
    resp = get(f"map/paused/{_running_map}")
    if resp.status_code != 200:
        show_error(resp.text)
        em.onmapstop()
        return True, False, False, False
    d = resp.json()
    if d['paused']:
        gather_show_outputs()
        return False, False, False, True

    # check to see if the map is done
    resp = get(f"map/done/{_running_map}")
    if resp.status_code != 200:
        show_error(resp.text)
        em.onmapstop()
        return True, False, False, False
    d = resp.json()
    if d['done']:
        return False, True, False, False

    # check to see if the map is blocked
    resp = get(f"map/blocked/{_running_map}")
    if resp.status_code != 200:
        show_error(resp.text)
        em.onmapstop()
        return True, False, False, False
    d = resp.json()
    blocked = d['blocked']
    if blocked:
        return False, False, True, False
    return False, False, False, False


@funlog
def continue_map(*args, **kwargs):
    # Watch the map for a configurable number of iterations.
    # If it is not done or blocked after that number of looks,
    # prompt the user to loave it running in the background...
    blocked = False
    done = False
    cnt = 0
    while cnt < CONFIG_MAP_TIMEOUT:
        just_stop, done, blocked, paused = check_map_runstatus()
        if just_stop:
            return
        time.sleep(CONFIG_ITER_WAITTIME)
        cnt += 1

        if blocked:
            # check for i/o
            # if input is needed, request it from the appropriate visible
            resp = get(f"input/{_running_map}")
            if resp.status_code != 200:
                show_error(resp.text)
                em.onmapstop()
                return

            d = resp.json()
            if len(d['wants_input']) > 0:
                for nodeid in d['wants_input']:
                    fetch_and_send_input(nodeid)
        elif done:
            gather_show_outputs()
            undim_map()
            clear_injected_tooltippers()
            em.onmapstop()
            return


@funlog
def end_debugmode(*args, **kwargs):
    getattr(em, 'key-control-d')(args, kwargs)
    stop_and_remove_map(args, kwargs)


@funlog
def stop_and_remove_map(*args, **kwargs):
    unload_map()
    undim_map()
    clear_injected_tooltippers()
    em.onmapstop()


@funlog
def load_map(data):
    global _running_map
    if _running_map is not None:
        if not unload_map():
            return None

    resp = post("map", data)

    if resp.status_code != 200:
        show_error(resp.text)
        return None

    d = resp.json()
    _running_map = d["guid"]
    return _running_map


@funlog
def unload_map():
    global _running_map
    resp = delete(f"map/{_running_map}")
    if resp.status_code != 200:
        show_error(resp.text)
        return False
    _running_map = None
    return True


@funlog
def run_debug(*args, **kwargs):
    # em.onmapstart()
    dim_map()
    global _running_map, _last_node
    # load_map is to be called upon entering debug mode such that
    # breakpoints can be communicated
    # data = Map.get_map_dict()  # get the current map.
    # rv = load_map(data)
    # if rv is None:
    #     return
    inject_dbg_tooltippers(_running_map)

    # Start the map
    resp = put(f"map/start/{_running_map}", data=[])

    if resp.status_code != 200:
        show_error(resp.text)
        return

    # d = resp.json()
    # _last_node = om.fetch(d['id'])
    # _last_node.set_undimmed()
    # _last_node.move_cam_to()

    render_exec_path()


@funlog
def stepnext(*args, **kwargs):
    global _running_map, _last_node
    if _running_map is None:
        return

    resp = get(f"map/done/{_running_map}")
    debug(f"GET /map/done/{_running_map} responded {resp.status_code}")

    if resp.status_code != 200:
        show_error(resp.text)
        em.onmapstop()
        return

    d = resp.json()
    if d["done"]:
        undim_map()
        clear_injected_tooltippers()
        if not unload_map():
            return
        em.onmapstop()
        return

    resp = put(f"next/{_running_map}")
    debug(f"PUT /next/{_running_map} responded {resp.status_code}")
    if resp.status_code != 200:
        show_error(resp.text)
        em.onmapstop()
        return

    node = resp.json()

    if 'id' in node:
        resp = get(f"node/{_running_map}/{node['id']}")
        debug(f"GET /node/{_running_map}/{node['id']} responded {resp.status_code}")
        if resp.status_code != 200:
            show_error(resp.text)
            em.onmapstop()
            return

        # check to see if the map is blocked
        resp = get(f"map/blocked/{_running_map}")
        if resp.status_code != 200:
            show_error(resp.text)
            em.onmapstop()
            return
        d = resp.json()

        _, executed = get_node_dbg_info("", _running_map, _last_node)
        if executed:
            # _last_node.set_dimmed(DEFAULTDBGEXECDALPHA)
            _last_node.renderSelected()
        else:
            _last_node.renderUnselected()
            # _last_node.set_dimmed()

        for rule in _last_node.mapentry.get_source_rules():
            for edge in _last_node.mapentry.get_sources(rule):
                _, executed = get_edge_dbg_info("", _running_map, edge)
                if hasattr(edge, 'visualization'):
                    if executed:
                        edge.visualization.renderSelected()
                    else:
                        edge.visualization.renderUnselected()

        for rule in _last_node.mapentry.get_target_rules():
            for edge in _last_node.mapentry.get_targets(rule):
                _, executed = get_edge_dbg_info("", _running_map, edge)
                if hasattr(edge, 'visualization'):
                    if executed:
                        edge.visualization.renderSelected()
                    else:
                        edge.visualization.renderUnselected()

        if d["blocked"]:

            resp = get(f"input/{_running_map}")
            if resp.status_code != 200:
                show_error(resp.text)
                em.onmapstop()
                return

            d = resp.json()
            if len(d['wants_input']) > 0:
                for nodeid in d['wants_input']:
                    fetch_and_send_input(nodeid, indebug=True)
                return
        else:

            if not om.has(node['id']):
                if 'position' in node:
                    move_cam_to(node['position'])
            else:
                node = om.fetch(node['id'])
                node.set_undimmed()
                node.move_cam_to()

                _last_node = node

    gather_show_outputs()
